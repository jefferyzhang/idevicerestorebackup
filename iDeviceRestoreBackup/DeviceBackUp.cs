﻿using Microsoft.Win32;
using NamePipe;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Management;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace iDeviceRestoreBackup
{
    public enum PipeType
    {
        BackUp,
        Restore
    }
    public class DeviceBackUp
    {
        public delegate void ProgressMessageHandler(string messID, params object[] parms);

        public delegate void ProgressValueHandler(int provalue);

        public delegate void ProgressErrorHandler(string messID, params object[] parms);

        public delegate void ProgressCompleteHandler();

        private string _deviceUUID = "";

        private string _sourceDeviceUUID = "";

        private string _pipename = "";

        //private NamedPipeClientStream _pipeClient;

        //private NamedPipeServerStream _pipeServer;

        private PipeType _pipetype;

        private bool _isAppRestore;

        private List<string> _appKeys;

        private string _appTempPath = "";

        private string _backupPath;

        private uint outBuffer = 1024u;

        private uint inBuffer = 1024u;

        private const int MAX_READ_BYTES = 5000;

        private ServerPipeConnection PipeConnection;

        public event DeviceBackUp.ProgressMessageHandler ProgressMessageEvent;

        public event DeviceBackUp.ProgressValueHandler ProgressValueEvent;

        public event DeviceBackUp.ProgressErrorHandler ProgressErrorEvent;

        public event DeviceBackUp.ProgressCompleteHandler ProgressCompleteEvent;
        public string BackupPath
        {
            get
            {
                return this._backupPath;
            }
            set
            {
                this._backupPath = value;
            }
        }

        public void SetLabel(String s)
        {
            _pipename = String.Format("{0}_{1}_{2}", Process.GetCurrentProcess().Id, Environment.TickCount, s);
        }
        public DeviceBackUp(string DeviceUUID, PipeType pipetype)
        {
            this._deviceUUID = DeviceUUID;
            this._pipetype = pipetype;
        }

        public DeviceBackUp(string targetDeviceUUID, PipeType pipetype, string sourceDeviceUUID)
        {
            this._deviceUUID = targetDeviceUUID;
            this._pipetype = pipetype;
            this._sourceDeviceUUID = sourceDeviceUUID;
        }

        public DeviceBackUp(string targetDeviceUUID, PipeType pipetype, bool isAppRestore, List<string> appKeys)
        {
            this._deviceUUID = targetDeviceUUID;
            this._pipetype = pipetype;
            this._isAppRestore = isAppRestore;
            this._appKeys = appKeys;
            this._appTempPath = Path.GetTempPath();
        }

        public void PrepareBackUp()
        {
            if (string.IsNullOrEmpty(this.BackupPath))
            {
                this.PrepareBeforeBackup();
            }
            if (!string.IsNullOrEmpty(this._deviceUUID))
            {
                this.BackupThread();
                return;
            }
            LogMessager.WriteInfo("Did not get the device's uuid!");
            throw new Exception("Device UUID is invalid!");
        }

        private void PrepareBeforeBackup()
        {
            string text = "";
            try
            {
                text = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            }
            catch (Exception ex)
            {
                LogMessager.WriteInfo("SHGetSpecialFolderPath Error," + ex.Message);
                text = "";
            }
            if (!string.IsNullOrEmpty(text))
            {
                string text2 = Path.Combine(text, "Apple Computer\\MobileSync\\Backup");
                if (!Directory.Exists(text2))
                {
                    Directory.CreateDirectory(text2);
                }
                this.BackupPath = text2;
            }
        }

        private void BackupThread()
        {
            new Thread(new ThreadStart(this.SendData))
            {
                IsBackground = true
            }.Start();
        }

        private void SendData()
        {
            try
            {
                string appleMobileBackupPath = this.GetAppleMobileBackupPath();
                if (string.IsNullOrEmpty(appleMobileBackupPath))
                {
                    throw new Exception("Mobile Device Support Path does not exist!");
                }
                if (!File.Exists(appleMobileBackupPath))
                {
                    throw new Exception("AppleMobileBackup.exe not exist!");
                }
                //this.KilliMobieBackUp();
                this.ClientProcessCommunication();
                this.StartiMobieBackUp(appleMobileBackupPath);
            }
            catch (Exception ex)
            {
                LogMessager.WriteInfo(string.Format("StackTrace:{0}---Message:{1}", ex.StackTrace, ex.Message));
            }
        }

        private string GetAppleMobileBackupPath()
        {
            object obj = null;
            try
            {
                obj = Registry.GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Apple Inc.\\Apple Mobile Device Support\\Shared", "AirTrafficHostDLL", null);
            }
            catch (Exception ex)
            {
                obj = null;
                LogMessager.WriteInfo(string.Format("StackTrace:{0}---Message:{1}", ex.StackTrace, ex.Message));
            }
            string text2;
            if (obj != null)
            {
                string text = obj.ToString();
                if (!string.IsNullOrEmpty(text))
                {
                    text2 =Regex.Replace(text, "AirTrafficHost.dll", "AppleMobileBackup.exe", RegexOptions.IgnoreCase);
                    if (!File.Exists(text2))
                    {
                        text2 = text2.Replace("Program Files", "Program Files (x86)");
                    }
                }
                else
                {
                    text2 = "";
                }
            }
            else
            {
                text2 = "";
            }
            return text2;
        }

        public void KilliMobieBackUp()
        {
            try
            {
                Process[] processesByName = Process.GetProcessesByName("AppleMobileBackup");
                if (processesByName != null && processesByName.Length != 0)
                {
                    Process[] array = processesByName;
                    for (int i = 0; i < array.Length; i++)
                    {
                        array[i].Kill();
                    }
                }
            }
            catch (Exception ex)
            {
                LogMessager.WriteInfo("Kill iMobie Backup Failed." + ex.ToString());
            }
        }

        private StringBuilder CreateSendDataPlist()
        {
            StringBuilder expr_05 = new StringBuilder();
            expr_05.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            expr_05.AppendLine("<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">");
            expr_05.AppendLine("<plist version=\"1.0\">");
            expr_05.AppendLine("<dict>");
            expr_05.AppendLine("<key>AMSDeviceIdentifierKey</key>\r\n<string>-1</string>\r\n<key>AMSMessageSignatureKey</key>");
            expr_05.AppendLine("<array>\r\n<string>AMSDeviceIdentifierKey</string>\r\n<string>AMSTargetIdentifierKey</string>\r\n<string>AMSBackupApplicationsKey</string>\r\n</array>");
            expr_05.AppendLine("<key>AMSRequestKey</key>\r\n<string>AMSBackupRequest</string>");
            expr_05.AppendLine("<key>AMSBackupApplicationsKey</key>");
            expr_05.AppendLine("<dict>");
            expr_05.AppendLine("<key>RootDirectory</key>");
            expr_05.AppendLine(string.Format("<string>{0}</string>", this.BackupPath));
            expr_05.AppendLine("</dict>");
            expr_05.AppendLine("<key>AMSTargetIdentifierKey</key>");
            expr_05.AppendLine(string.Format("<string>{0}</string>",this._deviceUUID));
            expr_05.AppendLine("</dict>");
            expr_05.AppendLine("</plist>");
            expr_05.Clear();

            String bf = @"<?xml version=""1.0"" encoding=""UTF-8""?>
<!DOCTYPE plist PUBLIC ""-//Apple//DTD PLIST 1.0//EN"" ""http://www.apple.com/DTDs/PropertyList-1.0.dtd"">
<plist version=""1.0"">
<dict>
    <key>AMSDeviceIdentifierKey</key>
    <string>-1</string>
    <key>AMSMessageSignatureKey</key>
    <array>
        <string>AMSDeviceIdentifierKey</string>
        <string>AMSTargetIdentifierKey</string>
        <string>AMSBackupOptionsKey</string>
    </array>
    <key>AMSRequestKey</key>
    <string>AMSBackupRequest</string>
    <key>AMSBackupOptionsKey</key>
    <dict>
        <key>RootDirectory</key>
        <string>{0}</string>
    </dict>
    <key>AMSTargetIdentifierKey</key>
    <string>{1}</string>
</dict>
</plist>";
            expr_05.AppendFormat(bf, BackupPath, _deviceUUID);
            return expr_05;
        }

        private StringBuilder CreateSendRestoreDataPlist()
        {
            StringBuilder stringBuilder = new StringBuilder();
            String rpold=@"<?xml version=""1.0"" encoding=""UTF-8""?>
<!DOCTYPE plist PUBLIC ""-//Apple//DTD PLIST 1.0//EN"" ""http://www.apple.com/DTDs/PropertyList-1.0.dtd"">
<plist version=""1.0"">
<dict>
    <key>AMSDeviceIdentifierKey</key>
    <string>-1</string>
    <key>AMSMessageSignatureKey</key>
    <array>
        <string>AMSDeviceIdentifierKey</string>
        <string>AMSTargetIdentifierKey</string>
        <string>AMSSourceTargetIdentifierKey</string>
        <string>AMSBackupApplicationsKey</string>
        <string>AMSRestoreRestoreOptionsKey</string>
    </array>
    <key>AMSRequestKey</key>
    <string>AMSRestoreWithApplicationsRequest</string>
    <key>AMSRestoreRestoreOptionsKey</key>
    <dict>
        <key>NotifySpringBoard</key>
        <true/>
        <key>RestorePreserveSettings</key>
        <true/>
        <key>RestoreDontCopyBackup</key>
        <true/>
        <key>RestorePreserveCameraRoll</key>
        <false/>
        <key>ShouldPerformSplitRestore</key>
        <false/>
        <key>ShouldPreserveSettingsAfterRestore</key>
        <false/>
        <key>VerifyDigests</key>
        <false/>
        <key>RootDirectory</key>
        <string>{0}</string>
    </dict>
    <key>AMSSourceTargetIdentifierKey</key>
    <string>{1}</string>
    <key>AMSTargetIdentifierKey</key>
    <string>{2}</string>
</dict>
</plist>";
            /*
             * if Source not exist, return -19
             */
            //stringBuilder.AppendFormat(rpold, BackupPath, string.IsNullOrEmpty(_sourceDeviceUUID) ? _deviceUUID : _sourceDeviceUUID, _deviceUUID);

            String rf = @"<?xml version=""1.0"" encoding=""UTF-8""?>
<!DOCTYPE plist PUBLIC ""-//Apple//DTD PLI ST 1.0//EN"" ""http://www.apple.com/DTDs/PropertyList-1.0.dtd"">
<plist version=""1.0"">
<dict>
    <key>AMSDeviceIdentifierKey</key>
    <string>-1</string>
    <key>AMSMessageSignatureKey</key>
    <array>
        <string>AMSDeviceIdentifierKey</string>
        <string>AMSTargetIdentifierKey</string>
        <string>AMSSourceTargetIdentifierKey</string>
        <string>AMSBackupApplicationsKey</string>
        <string>AMSRestoreRestoreOptionsKey</string>
    </array>
    <key>AMSRequestKey</key>
    <string>AMSRestoreWithApplicationsRequest</string>
    <key>AMSRestoreRestoreOptionsKey</key>
    <dict>
        <key>BackupAfterMigrate</key>
        <false/>
        <key>RestoreDontCopyBackup</key>
        <true/>
        <key>RootDirectory</key>
        <string>{0}</string>
    </dict>
    <key>AMSSourceTargetIdentifierKey</key>
    <string>{1}</string>
    <key>AMSTargetIdentifierKey</key>
    <string>{2}</string>
</dict>
</plist>";

            stringBuilder.AppendFormat(rf, BackupPath,  string.IsNullOrEmpty(_sourceDeviceUUID) ? _deviceUUID : _sourceDeviceUUID, _deviceUUID);

            return stringBuilder;
        }

        private StringBuilder CreateAppSendRestoreDataPlist()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n");
            stringBuilder.Append("<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">\r\n");
            stringBuilder.Append("<plist version=\"1.0\">\r\n");
            stringBuilder.Append("<dict>");
            stringBuilder.Append("<key>AMSBackupApplicationsKey</key>");
            stringBuilder.Append("<array>");
            if (this._appKeys != null && this._appKeys.Count > 0)
            {
                foreach (string current in this._appKeys)
                {
                    stringBuilder.Append("<string>");
                    stringBuilder.Append(current);
                    stringBuilder.Append("</string>\r\n");
                }
            }
            stringBuilder.Append("</array>");
            stringBuilder.Append("<key>AMSDeviceIdentifierKey</key>");
            stringBuilder.Append("<string>-1</string>");
            stringBuilder.Append("<key>AMSMessageSignatureKey</key>");
            stringBuilder.Append("<array>");
            stringBuilder.Append("<string>AMSDeviceIdentifierKey</string>");
            stringBuilder.Append("<string>AMSTargetIdentifierKey</string>");
            stringBuilder.Append("<string>AMSSourceTargetIdentifierKey</string>");
            stringBuilder.Append("<string>AMSBackupApplicationsKey</string>");
            stringBuilder.Append("<string>AMSRestoreRestoreOptionsKey</string>");
            stringBuilder.Append("</array>");
            stringBuilder.Append("<key>AMSRequestKey</key>");
            stringBuilder.Append("<string>AMSRestoreWithApplicationsRequest</string>");
            stringBuilder.Append("<key>AMSRestoreRestoreOptionsKey</key>");
            stringBuilder.Append("<dict>");
            stringBuilder.Append("<key>NotifySpringBoard</key>");
            stringBuilder.Append("<true/>");
            stringBuilder.Append("<key>RemoveItemsNotRestored</key>");
            stringBuilder.Append("<false/>");
            stringBuilder.Append("<key>RestoreDontCopyBackup</key>");
            stringBuilder.Append("<true/>");
            stringBuilder.Append("<key>RestorePreserveSettings</key>");
            stringBuilder.Append("<true/>");
            stringBuilder.Append("<key>RestoreShouldReboot</key>");
            stringBuilder.Append("<true/>");
            stringBuilder.Append("<key>RestoreSystemFiles</key>");
            stringBuilder.Append("<false/>");
            stringBuilder.Append("<key>RootDirectory</key>");
            if (!string.IsNullOrEmpty(this._appTempPath))
            {
                stringBuilder.Append("<string>");
                stringBuilder.Append(this._appTempPath);
                stringBuilder.Append("</string>\r\n");
            }
            stringBuilder.Append("<key>ShouldPerformSplitRestore</key>");
            stringBuilder.Append("<false/>");
            stringBuilder.Append("<key>ShouldPreserveSettingsAfterRestore</key>");
            stringBuilder.Append("<false/>");
            stringBuilder.Append("<key>VerifyDigests</key>");
            stringBuilder.Append("<false/>");
            stringBuilder.Append("</dict>");
            stringBuilder.Append("<key>AMSSourceTargetIdentifierKey</key>\r\n<string>");
            if (!string.IsNullOrEmpty(this._sourceDeviceUUID))
            {
                stringBuilder.Append(this._sourceDeviceUUID);
            }
            else
            {
                stringBuilder.Append(this._deviceUUID);
            }
            stringBuilder.Append("</string>\r\n");
            stringBuilder.Append("<key>AMSTargetIdentifierKey</key>\r\n<string>");
            stringBuilder.Append(this._deviceUUID);           
            stringBuilder.Append("</string>\r\n");
            stringBuilder.Append("</dict>");
            stringBuilder.Append("</plist>");
            return stringBuilder;
        }

        private bool CheckBackUpExeStartup()
        {
            bool flag = false;
            int num = 0;
            do
            {
                num++;
                if (num >= 10)
                {
                    break;
                }
                using (ManagementObjectSearcher managementObjectSearcher = new ManagementObjectSearcher("Select * From Win32_Process Where Name=\"AppleMobileBackup.exe\""))
                {
                    using (ManagementObjectCollection managementObjectCollection = managementObjectSearcher.Get())
                    {
                        if (managementObjectCollection.Count > 0)
                        {
                            flag = true;
                            break;
                        }
                    }
                }
            }
            while (!flag);
            return flag;
        }

        private void ClientProcessCommunication()
        {
            string name = _pipename;
            this.PipeConnection = new ServerPipeConnection(name, this.outBuffer, this.inBuffer, 5000, false);
            new Thread(new ThreadStart(this.PipeListener))
            {
                IsBackground = true
            }.Start();
        }

        private void PipeListener()
        {
            if (this.PipeConnection != null)
            {
                this.PipeConnection.Connect();
                StringBuilder stringBuilder = null;
                if (this._pipetype == PipeType.BackUp)
                {
                    stringBuilder = this.CreateSendDataPlist();
                    LogMessager.WriteInfo(stringBuilder.ToString());
                }
                else if (this._isAppRestore)
                {
                    stringBuilder = this.CreateAppSendRestoreDataPlist();
                    LogMessager.WriteInfo(stringBuilder.ToString());
                }
                else
                {
                    stringBuilder = this.CreateSendRestoreDataPlist();
                    LogMessager.WriteInfo(stringBuilder.ToString());
                }
                if (stringBuilder == null || stringBuilder.Length <= 0)
                {
                    this.PipeConnection.Disconnect();
                    this.PipeConnection.Close();
                    throw new Exception("Create Send Data Error!");
                }
                bool flag = false;
                try
                {
                    this.PipeConnection.Write(stringBuilder.ToString());
                    this.CreateReadPipeThread();
                    flag = true;
                }
                catch (Exception ex)
                {
                    flag = false;
                    LogMessager.WriteInfo("PipeConnection backup exception:" + ex.Message);
                }
                if (!flag)
                {
                    this.TryConnectionPipe(stringBuilder.ToString());
                    return;
                }
            }
        }

        private void TryConnectionPipe(string sendCommand)
        {
            if (this.PipeConnection != null)
            {
                bool flag = false;
                try
                {
                    this.PipeConnection.Disconnect();
                    this.PipeConnection.Close();
                    this.PipeConnection.Connect();
                    this.PipeConnection.Write(sendCommand);
                    this.CreateReadPipeThread();
                    flag = true;
                }
                catch (Exception ex)
                {
                    flag = false;
                    LogMessager.WriteInfo("TryConnectionPipe backup exception:" + ex.Message);
                }
                if (!flag && this.ProgressErrorEvent != null)
                {
                    this.ProgressErrorEvent("BackUp Failed. errorCode:-100", new object[]
					{
						-100
					});
                }
            }
        }

        private ProcessStartInfo StartiMobieBackUp(string imobieBackUpPath)
        {
            LogMessager.WriteInfo(string.Format("StartiMobieBackUp method. \nbackupPath:{0}", imobieBackUpPath));
            NamedPipeNative.WaitNamedPipe(String.Format("\\\\.\\pipe\\{0}", _pipename), 5000);
            ProcessStartInfo processStartInfo = null;
            try
            {
                //imobieBackUpPath = @"D:\Works\iTunes\version_11.1.3.8\FakeBackupRestore\FakeBackupRestore\bin\Debug\FakeBackupRestore.exe";
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                processStartInfo = new ProcessStartInfo(imobieBackUpPath);
                processStartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                processStartInfo.Arguments = String.Format("--pipe \\\\.\\pipe\\{0}", _pipename);
                processStartInfo.RedirectStandardOutput = true;
                processStartInfo.RedirectStandardError = true;
                processStartInfo.UseShellExecute = false;
                p.StartInfo = processStartInfo;
                p.OutputDataReceived += (sender, e) =>
                {
                    if (!string.IsNullOrEmpty(e.Data))
                    {
                        LogMessager.WriteInfo(string.Format("[runEXE]: {0}", e.Data));            
                    }
                };
                p.ErrorDataReceived += (sender, e) =>
                {
                    if (!string.IsNullOrEmpty(e.Data))
                    {
                        LogMessager.WriteInfo(string.Format("[runEXE]: {0}", e.Data));                        
                    }
                };
                p.Start();
                p.BeginErrorReadLine();
                p.BeginOutputReadLine();
                //Process.Start(processStartInfo);
                LogMessager.WriteInfo("AppleMobileBackup.exe is opened.");
                p.WaitForExit();
                LogMessager.WriteInfo(String.Format("AppleMobileBackup.exe exit errorcode= {0}", p.ExitCode));
            }
            catch (Exception ex)
            {
                LogMessager.WriteInfo("Startup mobilebackup process exception:" + ex.Message);
                if (this.PipeConnection != null)
                {
                    this.PipeConnection.Disconnect();
                    this.PipeConnection.Close();
                }
            }
            return processStartInfo;
        }

        private void CreateReadPipeThread()
        {
            new Thread(new ThreadStart(this.ReceiveDataFromClient))
            {
                IsBackground = true
            }.Start();
        }

        private void ReceiveDataFromClient()
        {
            try
            {
                string text;
                do
                {
                    text = this.PipeConnection.Read();
                }
                while (string.IsNullOrEmpty(text) || this.AnalyzeBackUpPlist(text));
            }
            catch (Exception)
            {
            }
            finally
            {
                DeviceBackUp.ProgressMessageHandler arg_28_0 = this.ProgressMessageEvent;
                if (this.PipeConnection != null)
                {
                    this.PipeConnection.Disconnect();
                    this.PipeConnection.Close();
                }
                if (this._pipetype == PipeType.BackUp || this._pipetype == PipeType.Restore)
                {
                    Thread.Sleep(500);
                    if (this.ProgressCompleteEvent != null)
                    {
                        this.ProgressCompleteEvent();
                    }
                }
            }
        }

        private bool AnalyzeBackUpPlist(string linestr)
        {
            bool result = false;
            if (string.IsNullOrEmpty(linestr))
            {
                return false;
            }
            object obj = null;
            try
            {
                obj = PListReadWrite.readPlist(Encoding.Default.GetBytes(linestr));
            }
            catch (Exception ex)
            {
                LogMessager.WriteInfo(string.Format("StackTrace:{0}---Message:{1}", ex.StackTrace, ex.Message));
            }
            if (obj != null && obj is Dictionary<string, object>)
            {
                Dictionary<string, object> dictionary = obj as Dictionary<string, object>;
                if (dictionary.ContainsKey("AMSBackupPercentageKey"))
                {
                    string value = "0";
                    try
                    {
                        value = ((dictionary["AMSBackupPercentageKey"] != null) ? dictionary["AMSBackupPercentageKey"].ToString() : "0");
                    }
                    catch
                    {
                    }
                    if (!string.IsNullOrEmpty(value))
                    {
                        this.ProgressValueEvent(Convert.ToInt32(value));
                        result = true;
                    }
                }
                else if (dictionary.ContainsKey("errorcode") || dictionary.ContainsKey("errorCode"))
                {
                    string key = "errorCode";
                    if (dictionary.ContainsKey("errorcode"))
                    {
                        key = "errorcode";
                    }
                    else if (dictionary.ContainsKey("errorCode"))
                    {
                        key = "errorCode";
                    }
                    if (this.ProgressErrorEvent != null)
                    {
                        string text = "-2";
                        try
                        {
                            text = ((dictionary[key] != null) ? dictionary[key].ToString() : "");
                        }
                        catch
                        {
                            text = "-2";
                        }
                        if (this._pipetype == PipeType.BackUp)
                        {
                            this.ProgressErrorEvent("BackUp Failed. errorCode:" + text, new object[]
							{
								text
							});
                        }
                        else
                        {
                            this.ProgressErrorEvent("Restore Failed.errorCode:" + text, new object[]
							{
								text
							});
                        }
                    }
                    result = false;
                }
                else
                {
                    if (this.ProgressMessageEvent != null)
                    {
                        if (this._pipetype == PipeType.BackUp)
                        {
                            this.ProgressMessageEvent("Backup_id_10", null);
                        }
                        else
                        {
                            this.ProgressMessageEvent("Restore complete!", null);
                        }
                    }
                    result = false;
                }
            }
            return result;
        }
    }
}
