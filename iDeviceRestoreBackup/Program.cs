﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace iDeviceRestoreBackup
{
    class Program
    {
        private static DeviceBackUp deviceBackupTool;
        private static EventWaitHandle waittask =
            new EventWaitHandle(false, EventResetMode.AutoReset);

        static int ret = -1;

        static private void BackupDevice(string sourceBackupUUID, string backupPath, string label)
        {
            try
            {
                LogMessager.WriteInfo("----restore----:Merge Backup device ");
                deviceBackupTool = new DeviceBackUp(sourceBackupUUID, PipeType.BackUp);
                deviceBackupTool.SetLabel(label);
                if (!string.IsNullOrEmpty(backupPath))
                {
                    if (!Directory.Exists(backupPath))
                    {
                        Directory.CreateDirectory(backupPath);
                    }
                    deviceBackupTool.BackupPath = backupPath;
                    deviceBackupTool.ProgressValueEvent += new DeviceBackUp.ProgressValueHandler(sourceDeviceBackupTool_ProgressValueEvent);
                    deviceBackupTool.ProgressErrorEvent += new DeviceBackUp.ProgressErrorHandler(sourceDeviceBackupTool_ProgressErrorEvent);
                    deviceBackupTool.ProgressCompleteEvent += new DeviceBackUp.ProgressCompleteHandler(sourceDeviceBackupTool_ProgressCompleteEvent);
                    deviceBackupTool.PrepareBackUp();
                }
            }
            catch (Exception ex)
            {
                LogMessager.WriteInfo("----error----:Merge restore device unknown error:" + ex.Message.ToString());
            }
        }
        static private void RestoreDevice(string targetUUID, string sourceBackupUUID, string backupPath, string label)
        {
            try
            {
                LogMessager.WriteInfo("----restore----:Merge Backup device ");
                deviceBackupTool = new DeviceBackUp(targetUUID, PipeType.Restore, sourceBackupUUID);
                deviceBackupTool.SetLabel(label);
                if (!string.IsNullOrEmpty(backupPath))
                {
                    if (!Directory.Exists(backupPath))
                    {
                        Directory.CreateDirectory(backupPath);
                    }
                    deviceBackupTool.BackupPath = backupPath;
                    deviceBackupTool.ProgressValueEvent += new DeviceBackUp.ProgressValueHandler(sourceDeviceBackupTool_ProgressValueEvent);
                    deviceBackupTool.ProgressErrorEvent += new DeviceBackUp.ProgressErrorHandler(sourceDeviceBackupTool_ProgressErrorEvent);
                    deviceBackupTool.ProgressCompleteEvent += new DeviceBackUp.ProgressCompleteHandler(sourceDeviceBackupTool_ProgressCompleteEvent);
                    deviceBackupTool.PrepareBackUp();
                }
            }
            catch (Exception ex)
            {
                LogMessager.WriteInfo("----error----:Merge restore device unknown error:" + ex.Message.ToString());
            }
        }

        private static void sourceDeviceBackupTool_ProgressCompleteEvent()
        {
            LogMessager.WriteInfo(string.Format("sourceDeviceBackupTool_ProgressCompleteEvent method"));
            if (deviceBackupTool != null)
            {
                deviceBackupTool.ProgressValueEvent -= new DeviceBackUp.ProgressValueHandler(sourceDeviceBackupTool_ProgressValueEvent);
                deviceBackupTool.ProgressErrorEvent -= new DeviceBackUp.ProgressErrorHandler(sourceDeviceBackupTool_ProgressErrorEvent);
                deviceBackupTool.ProgressCompleteEvent -= new DeviceBackUp.ProgressCompleteHandler(sourceDeviceBackupTool_ProgressCompleteEvent);
            }
            ret = 0;
            waittask.Set();
        }

        private static void sourceDeviceBackupTool_ProgressErrorEvent(string messID, params object[] parms)
        {
            LogMessager.WriteInfo(string.Format("sourceDeviceBackupTool_ProgressErrorEvent method."));
            waittask.Set();
            string text = (parms != null) ? parms[0].ToString() : "";
            LogMessager.WriteInfo(string.Format("Backup Device Data Eorror:{0},errocode:{1}", messID, text));
            try
            {
                ret = Convert.ToInt32(text);
            }
            catch (Exception) { }

            if (deviceBackupTool != null)
            {
                deviceBackupTool.ProgressValueEvent -= new DeviceBackUp.ProgressValueHandler(sourceDeviceBackupTool_ProgressValueEvent);
                deviceBackupTool.ProgressErrorEvent -= new DeviceBackUp.ProgressErrorHandler(sourceDeviceBackupTool_ProgressErrorEvent);
            }
            
            if (text == "-35")
            {
            }
            if (text == "-30")
            {
                deviceBackupTool.ProgressCompleteEvent -= new DeviceBackUp.ProgressCompleteHandler(sourceDeviceBackupTool_ProgressCompleteEvent);
            }

        }

        private static void sourceDeviceBackupTool_ProgressValueEvent(int provalue)
        {
            Console.WriteLine(Math.Round((double)provalue / 100.0, 2));
        }

        static void Usage()
        {
            Console.WriteLine("-label=1");
            Console.WriteLine("-udid=*******");
            Console.WriteLine("-restore[backup]");
            Console.WriteLine("-source=restore folder sub folder");
            Console.WriteLine("-path=restore folder");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
        }

        static int  Main(string[] args)
        {
            System.Configuration.Install.InstallContext _args = new System.Configuration.Install.InstallContext(null, args);
            if (_args.IsParameterTrue("debug"))
            {
                System.Console.WriteLine("Wait for debugger, press any key continue...");
                System.Console.ReadKey();
            }
            LogMessager.WriteInfo(string.Format("Dump args: ({0})", args.Length));
            foreach (string s in args)
            {
                LogMessager.WriteInfo(s);
            }

            Environment.SetEnvironmentVariable("AppleBackRestoreReal", "true");

            if (!_args.Parameters.ContainsKey("udid"))
            {
                ret = 2;
                Usage();
                return ret;
            }
            if(!_args.Parameters.ContainsKey("label"))
            {
                ret = 3;
                Usage();
                return ret;
            }
            Environment.SetEnvironmentVariable("DeviceUUID", _args.Parameters["udid"]);
            if (_args.IsParameterTrue("restore"))
            {
                if (!_args.Parameters.ContainsKey("source"))
                {
                    ret = 4;
                    return ret;
                }
                try
                {
                    RestoreDevice(_args.Parameters["udid"], _args.Parameters["source"], _args.Parameters["path"], _args.Parameters["label"]);
                }
                catch (Exception e)
                {
                    LogMessager.WriteInfo(e.ToString());
                }
            }
            else if(_args.IsParameterTrue("backup"))
            {
                try { 
                    BackupDevice(_args.Parameters["udid"], _args.Parameters["path"], _args.Parameters["label"]);
                }
                catch (Exception e) {
                    LogMessager.WriteInfo(e.ToString());
                }
            }
            else
            {
                Usage();
                LogMessager.WriteInfo("can not do task. commandline error.");
                return ret;
            }
            waittask.WaitOne();
            return ret;
        }
    }
}
