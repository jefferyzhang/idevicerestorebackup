#pragma once
#include <string>
typedef struct _tagPARAMETERS_T
{
	_tagPARAMETERS_T(){
		bVerbose = TRUE;
	};
	~_tagPARAMETERS_T()
	{
	}

	BOOL bVerbose;
	std::string udid;
}PARAMETERS_T, *PPARAMETERS_T;

extern PARAMETERS_T g_args;

void hook_iTunesMobileDevice_apis(HMODULE hLib);

// from util.cpp
void logIt(System::String^ msg);
wchar_t* StringToPointerW(System::String^ str, wchar_t* buf, int len);
char* StringToPointerA(System::String^ str, char* buf, int len);

// from fd_AppleMobileBackup.cpp
//int callLibrary(int argc, char** argv);
//char** prepareArgv(array<System::String ^> ^args);
int callAppleLibrary(array<System::String ^> ^args);

// from pipeComm.cpp
int start(System::String^ serverPipeName);

// from fake_client.cpp
int fake_start(System::String^ iTunesPipeName);
void fake_test();

#pragma pack(push,1)

typedef struct am_device_notification_callback_info {
	HANDLE deviceHandle;				/* 0    device */
	unsigned int msgType;					/* 4    one of ADNCI_MSG_* */
	struct am_device_notification* subscription;
}AMDeviceNotificationCallbackInformation, *AMDeviceNotificationCallbackInformationRef;

typedef void(__cdecl *AMDeviceNotificationCallback)(AMDeviceNotificationCallbackInformationRef, void* ctx);
enum ADNCI_MSG {
	ADNCI_MSG_CONNECTED = 1,
	ADNCI_MSG_DISCONNECTED = 2,
	ADNCI_MSG_UNSUBSCRIBED = 3
};
