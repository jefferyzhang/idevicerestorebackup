// main.cpp : main project file.
#using <System.Xml.dll>
#include "stdafx.h"
#include "AppleMobileBackup.h"
#include <string>

using namespace System;
using namespace System::Xml;
using namespace System::Runtime::InteropServices;

PARAMETERS_T g_args;

String^ getPipeName(array<System::String ^> ^args)
{
	String^ ret = "";
	for (int i = 0; i < args->Length; i++)
	{
		if (String::Compare(args[i], L"--pipe") == 0)
		{
			if (i+1 < args->Length)
				ret = args[i+1];
		}
		if (!String::IsNullOrEmpty(ret))
		{
			logIt(String::Format(L"found pipe name={0}", ret));
			break;
		}
	}
	return ret;
}
int main(array<System::String ^> ^args)
{
	int ret = 0;
	logIt(System::String::Format(L"fd_AppleMobileBackup: ++ command line: {0}", System::Environment::CommandLine));
	String^ server_pipe = getPipeName(args);
	//if (String::IsNullOrEmpty(server_pipe))
	String^ uuid = Environment::GetEnvironmentVariable("DeviceUUID");
	if (!String::IsNullOrEmpty(uuid))
	{
		char* str2 = (char*)(void*)Marshal::StringToHGlobalAnsi(uuid);
		g_args.udid = str2;
	}
	String^ isRealDo = Environment::GetEnvironmentVariable("AppleBackRestoreReal");
	if (!String::IsNullOrEmpty(isRealDo) || String::IsNullOrEmpty(server_pipe))
	{
		logIt(L"Called with command line parameters.");
		// called without pipe, parse command line
		ret = callAppleLibrary(args);
	}
	else
	{
		logIt(L"Called with pipe communicate.");
		// called with pipe.
		//ret = start(server_pipe);
		ret = fake_start(server_pipe);
	}
	logIt(System::String::Format(L"fd_AppleMobileBackup: -- ret={0}", ret));
	return ret;
}

int test_main(array<System::String ^> ^args)
{
	fake_test();
	return 0;
}