#include "stdafx.h"
#include <Dbghelp.h>
#include <string>
#include <CoreFoundation/CFString.h>

#include "AppleMobileBackup.h"

#pragma comment(lib,"Dbghelp.lib")

using namespace System;


PIMAGE_SECTION_HEADER GetEnclosingSectionHeader(DWORD rva, PIMAGE_NT_HEADERS pNTHeader)
{
	PIMAGE_SECTION_HEADER section = IMAGE_FIRST_SECTION(pNTHeader);
	unsigned i;
	for (i = 0; i < pNTHeader->FileHeader.NumberOfSections; i++, section++)
	{
		// This 3 line idiocy is because Watcom's linker actually sets the
		// Misc.VirtualSize field to 0.  (!!! - Retards....!!!)
		DWORD size = section->Misc.VirtualSize;
		if (0 == size)
			size = section->SizeOfRawData;

		// Is the RVA within this section?
		if ((rva >= section->VirtualAddress) &&
			(rva < (section->VirtualAddress + size)))
			return section;
	}

	return 0;
}

LPVOID GetPtrFromRVA(PIMAGE_NT_HEADERS pNTHeader, PVOID imageBase, ULONG rva)
{
	PIMAGE_SECTION_HEADER pSectionHdr;
	INT delta;

	pSectionHdr = GetEnclosingSectionHeader(rva, pNTHeader);
	if (!pSectionHdr)
		return 0;

	delta = (INT)(pSectionHdr->VirtualAddress - pSectionHdr->PointerToRawData);
	//return (LPVOID) ( (ULONG)imageBase + rva - delta );	
	return (LPVOID)((ULONG)imageBase + rva);
}

DWORD SetFunctionAddress(HMODULE hModule, LPCSTR szDllName, LPCSTR szFuncName, DWORD* lpOriginal, DWORD lpNewAddress)
{
	DWORD ret = ERROR_SUCCESS;
	if (hModule != NULL && lpOriginal != NULL && lpNewAddress != NULL && szDllName && strlen(szDllName))
	{
		HMODULE h = hModule;
		if (h != NULL)
		{
			PBYTE imageBase = (PBYTE)h;
			PIMAGE_DOS_HEADER dosHeader = (PIMAGE_DOS_HEADER)imageBase;
			PIMAGE_NT_HEADERS pNTHeader = ImageNtHeader(imageBase);//(PIMAGE_NT_HEADERS)((LPBYTE)dosHeader+dosHeader->e_lfanew);
			if (pNTHeader->Signature == 0x4550)
			{
				BOOL done = FALSE;
				//PIMAGE_SECTION_HEADER sectionHeader=ImageRvaToSection(pNTHeader,imageBase,pNTHeader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);
				PIMAGE_SECTION_HEADER sectionHeader = GetEnclosingSectionHeader(pNTHeader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress, pNTHeader);
				//PIMAGE_IMPORT_DESCRIPTOR pImportTable=(PIMAGE_IMPORT_DESCRIPTOR)ImageRvaToVa(pNTHeader,imageBase,pNTHeader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress,NULL);
				ULONG size = 0;
				PIMAGE_IMPORT_DESCRIPTOR pImportTable = (PIMAGE_IMPORT_DESCRIPTOR)ImageDirectoryEntryToData(imageBase, TRUE, IMAGE_DIRECTORY_ENTRY_IMPORT, &size);
				DWORD flOldProtect = 0;

				do
				{
					if (!(pImportTable->TimeDateStamp == 0 && pImportTable->Name == 0))
					{
						char* dllName = (char*)GetPtrFromRVA(pNTHeader, imageBase, pImportTable->Name);
						//logIt("Library: %s\n", dllName);
						if (_stricmp(dllName, szDllName) == 0)
						{
							PIMAGE_THUNK_DATA pINT = (PIMAGE_THUNK_DATA)GetPtrFromRVA(pNTHeader, imageBase, pImportTable->OriginalFirstThunk);
							PIMAGE_THUNK_DATA pIAT = (PIMAGE_THUNK_DATA)GetPtrFromRVA(pNTHeader, imageBase, pImportTable->FirstThunk);
							do
							{
								if (pINT->u1.AddressOfData != 0)
								{
									if (IMAGE_SNAP_BY_ORDINAL32(pINT->u1.Ordinal))
									{
										// it is a ordinal
									}
									else
									{
										PIMAGE_IMPORT_BY_NAME pOrdinalName = (PIMAGE_IMPORT_BY_NAME)GetPtrFromRVA(pNTHeader, imageBase, pINT->u1.AddressOfData);
										//logIt("Function: %s\n", pOrdinalName->Name);
										if (strcmp((char*)pOrdinalName->Name, szFuncName) == 0)
										{
											//if (!(sectionHeader->Characteristics&IMAGE_SCN_MEM_WRITE))
											{
												DWORD flOldProtect;
												//lpFuncAddress=(BOOL (WINAPI* )(HANDLE,LPCVOID,DWORD,LPDWORD,LPOVERLAPPED))pIAT->u1.AddressOfData;
												*lpOriginal = pIAT->u1.AddressOfData;
												if (VirtualProtect((LPVOID)&(pIAT->u1.AddressOfData), sizeof(DWORD), PAGE_READWRITE, &flOldProtect))
												{
													pIAT->u1.AddressOfData = lpNewAddress;
												}
											}


											done = TRUE;
										}
									}
									pIAT++;
									pINT++;
								}
								else break;
							} while (!done);
						}
						pImportTable++;
					}
					else break;
				} while (!done);
			}
		}
	}
	return ret;
}

DWORD SetFunctionAddressV2(HMODULE hModule, LPCSTR szDllName, short ordinal, DWORD* lpOriginal, DWORD lpNewAddress)
{
	DWORD ret = ERROR_SUCCESS;
	if (hModule != NULL && lpOriginal != NULL && ordinal > 0 && szDllName && strlen(szDllName))
	{
		HMODULE h = hModule;
		if (h != NULL)
		{
			PBYTE imageBase = (PBYTE)h;
			PIMAGE_DOS_HEADER dosHeader = (PIMAGE_DOS_HEADER)imageBase;
			PIMAGE_NT_HEADERS pNTHeader = ImageNtHeader(imageBase);//(PIMAGE_NT_HEADERS)((LPBYTE)dosHeader+dosHeader->e_lfanew);
			if (pNTHeader->Signature == 0x4550)
			{
				BOOL done = FALSE;
				//PIMAGE_SECTION_HEADER sectionHeader=ImageRvaToSection(pNTHeader,imageBase,pNTHeader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);
				PIMAGE_SECTION_HEADER sectionHeader = GetEnclosingSectionHeader(pNTHeader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress, pNTHeader);
				//PIMAGE_IMPORT_DESCRIPTOR pImportTable=(PIMAGE_IMPORT_DESCRIPTOR)ImageRvaToVa(pNTHeader,imageBase,pNTHeader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress,NULL);
				ULONG size = 0;
				PIMAGE_IMPORT_DESCRIPTOR pImportTable = (PIMAGE_IMPORT_DESCRIPTOR)ImageDirectoryEntryToData(imageBase, TRUE, IMAGE_DIRECTORY_ENTRY_IMPORT, &size);
				DWORD flOldProtect = 0;

				do
				{
					if (!(pImportTable->TimeDateStamp == 0 && pImportTable->Name == 0))
					{
						char* dllName = (char*)GetPtrFromRVA(pNTHeader, imageBase, pImportTable->Name);
						//logIt("Library: %s\n", dllName);
						if (_stricmp(dllName, szDllName) == 0)
						{
							PIMAGE_THUNK_DATA pINT = (PIMAGE_THUNK_DATA)GetPtrFromRVA(pNTHeader, imageBase, pImportTable->OriginalFirstThunk);
							PIMAGE_THUNK_DATA pIAT = (PIMAGE_THUNK_DATA)GetPtrFromRVA(pNTHeader, imageBase, pImportTable->FirstThunk);
							do
							{
								if (pINT->u1.AddressOfData != 0)
								{
									if (IMAGE_SNAP_BY_ORDINAL32(pINT->u1.Ordinal))
									{
										// it is a ordinal
										short ord = LOWORD(pINT->u1.Ordinal);
										if (ord == ordinal)
										{
											DWORD flOldProtect;
											//lpFuncAddress=(BOOL (WINAPI* )(HANDLE,LPCVOID,DWORD,LPDWORD,LPOVERLAPPED))pIAT->u1.AddressOfData;
											*lpOriginal = pIAT->u1.AddressOfData;
											if (VirtualProtect((LPVOID)&(pIAT->u1.AddressOfData), sizeof(DWORD), PAGE_READWRITE, &flOldProtect))
											{
												pIAT->u1.AddressOfData = lpNewAddress;
											}
											done = TRUE;
										}
									}
									else
									{
									}
									pIAT++;
									pINT++;
								}
								else break;
							} while (!done);
						}
						pImportTable++;
					}
					else break;
				} while (!done);
			}
		}
	}
	return ret;
}

FARPROC LoadApiFun(LPCTSTR dllName, LPCSTR ApiName)
{
	FARPROC lpFUN = NULL;
	HMODULE h = LoadLibrary(dllName);
	if (h != NULL)
	{
		lpFUN = GetProcAddress(h, ApiName);
	}
	return lpFUN;
}

AMDeviceNotificationCallback iTunes_DeviceNotificationCallback = NULL;
void deviceNotificationCallback(AMDeviceNotificationCallbackInformationRef info, void *user)
{
	if (iTunes_DeviceNotificationCallback == NULL)
	{
		return;
	}
	char c_value[512];
	if (info->msgType == ADNCI_MSG_CONNECTED)
	{
		FARPROC lpfun = LoadApiFun(_T("MobileDevice.dll"), "AMDeviceCopyDeviceIdentifier");
		if (lpfun != NULL)
		{
			CFStringRef udid = ((CFStringRef(__cdecl *)(HANDLE))lpfun)(info->deviceHandle);

			CFStringGetCString(udid, c_value, 510, kCFStringEncodingUTF8);
			if (g_args.udid.size() > 0)
			{
				if (_strnicmp(c_value, g_args.udid.c_str(), g_args.udid.size()) == 0)
				{
					//if is right call
					iTunes_DeviceNotificationCallback(info, user);
				}
			}
			else
			{
				iTunes_DeviceNotificationCallback(info, user);
			}
		}

	}
	else{
		//call old callback
		iTunes_DeviceNotificationCallback(info, user);
	}
}

int __cdecl hook_AMDeviceNotificationSubscribe(AMDeviceNotificationCallback cb, int a, int b, void *c, void** d)
{
	int ret = -1;
	HMODULE h = LoadLibrary(_T("MobileDevice.dll"));
	int(__cdecl* fn)(AMDeviceNotificationCallback, int, int, void *, void**) = (int(__cdecl*)(AMDeviceNotificationCallback, int, int, void *, void**))GetProcAddress(h, "AMDeviceNotificationSubscribe");
	if (fn != NULL)
	{
		ret = fn(deviceNotificationCallback, a, b, c, d);
		iTunes_DeviceNotificationCallback = cb;
	}
	return ret;
}


void hook_iTunesMobileDevice_apis(HMODULE hLib)
{
	if (hLib != NULL)
	{
		DWORD proc = 0; //iTunesMobileDevice!AMDeviceNotificationSubscribe
		if (SetFunctionAddress(hLib, "MobileDevice.dll", "AMDeviceNotificationSubscribe", &proc, (DWORD)hook_AMDeviceNotificationSubscribe) == ERROR_SUCCESS)
		{
		}
	}
}


void logIt(System::String^ msg)
{
	System::String^ s = System::String::Format(L"[AppleMobileBackup]: {0}", msg);
	System::Diagnostics::Trace::WriteLine(s);
}

wchar_t* StringToPointerW(System::String^ str, wchar_t* buf, int len)
{
	wchar_t* ret = NULL;
	if (!System::String::IsNullOrEmpty(str) && len>str->Length && buf != NULL)
	{
		System::IntPtr pointer = System::Runtime::InteropServices::Marshal::StringToHGlobalUni(str);
		if (pointer != System::IntPtr::Zero)
		{
			//const wchar_t* lpIniFileName = (wchar_t*)pointer.ToPointer();
			if (SUCCEEDED(StringCbCopyW(buf, len, (wchar_t*)pointer.ToPointer())))
			{
				ret = buf;
			}
			System::Runtime::InteropServices::Marshal::FreeHGlobal(pointer);
		}
	}
	return ret;
}

char* StringToPointerA(System::String^ str, char* buf, int len)
{
	char* ret = NULL;
	if (!System::String::IsNullOrEmpty(str) && len>str->Length && buf != NULL)
	{
		System::IntPtr pointer = System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(str);
		if (pointer != System::IntPtr::Zero)
		{
			//const wchar_t* lpIniFileName = (wchar_t*)pointer.ToPointer();
			if (SUCCEEDED(StringCbCopyA(buf, len, (char*)pointer.ToPointer())))
			{
				ret = buf;
			}
			System::Runtime::InteropServices::Marshal::FreeHGlobal(pointer);
		}
	}
	return ret;
}