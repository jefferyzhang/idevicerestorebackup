#include "stdafx.h"
#include "AppleMobileBackup.h"

using namespace System;

void logIO(String^ msg, String^ data)
{
	logIt(String::Format("{0}: {1}", msg, data));
}
DWORD WINAPI ThreadProc(LPVOID lpParameter)
{
	int ret = ERROR_INVALID_PARAMETER;
	char* pipeName = (char*)lpParameter;
	array<System::String ^> ^args = gcnew array<System::String ^>(2);
	args[0] = L"--pipe";
	args[1] = gcnew String(pipeName);
	ret = callAppleLibrary(args);
	return ret;
}

HANDLE waitForDllConnect(HANDLE hPipe, char* pipeName)
{
	logIt(L"waitForDllConnect: ++");
	HANDLE ret = NULL;
	DWORD err = ERROR_INVALID_PARAMETER;
	if (hPipe != INVALID_HANDLE_VALUE && pipeName != NULL)
	{
		OVERLAPPED o1;
		ZeroMemory(&o1, sizeof(OVERLAPPED));
		o1.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
		BOOL b = ConnectNamedPipe(hPipe, &o1);
		if (!b)
		{
			err = GetLastError();
			if (err == ERROR_IO_PENDING)
			{
				// here we can start the launch apple dll
				ret = CreateThread(NULL, 0, ThreadProc, pipeName, 0, &err);
				if (ret != NULL)
				{
					err = WaitForSingleObject(o1.hEvent, INFINITE);
					if (err == WAIT_OBJECT_0)
					{
						DWORD i;
						GetOverlappedResult(hPipe, &o1, &i, TRUE);
						logIt(gcnew String(L"accept_connection connected"));
					}
				}
			}
		}
		CloseHandle(o1.hEvent);
	}
	logIt(L"waitForDllConnect: -- ");
	return ret;
}

HANDLE connectToiTunesPipe(char* pipeName)
{
	logIt(String::Format(L"connectToiTunesPipe: {0}", gcnew String(pipeName)));
	HANDLE hMyClient = CreateFileA(pipeName, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, NULL);
	if (hMyClient != INVALID_HANDLE_VALUE)
	{
		DWORD d = PIPE_READMODE_MESSAGE;
		BOOL b = SetNamedPipeHandleState(hMyClient, &d, NULL, NULL);
	}
	else
	{
		logIt(String::Format("Fail to connect iTunes pipe: err={0}", GetLastError()));
	}
	return hMyClient;
}

DWORD write_pipe(HANDLE h, LPBYTE data, int len, int timeout=1000)
{
	DWORD ret = NO_ERROR;
	OVERLAPPED o1;
	ZeroMemory(&o1, sizeof(OVERLAPPED));
	o1.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	DWORD i;
	String^ s = gcnew String((char*)data, 0, len);
	logIt(String::Format(L"write_pipe: {0}", s));
	BOOL b = WriteFile(h, data, len, &i, &o1);
	if (!b)
	{
		ret = WaitForSingleObject(o1.hEvent, timeout);
		if (ret == WAIT_OBJECT_0)
		{
			b = GetOverlappedResult(h, &o1, &i, TRUE);
			if (b)
			{
				if (i == len)
				{
					// good.
					ret = NO_ERROR;
				}
				else
				{
					// error
				}
			}
			else
			{
				// error
			}
		}
		else
		{
			// wait fail 
		}
	}
	else
	{
		// write done.
	}
	CloseHandle(o1.hEvent);
	return ret;
}

int startProxy(HANDLE iTunePipe, HANDLE dllPipe, HANDLE hThread)
{
	// loop until hThread terminated
	BOOL done = FALSE;
	OVERLAPPED oiTunesPipe;
	OVERLAPPED oDllPipe;
	ZeroMemory(&oiTunesPipe, sizeof(OVERLAPPED));
	ZeroMemory(&oDllPipe, sizeof(OVERLAPPED));
	oiTunesPipe.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	oDllPipe.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	BOOL iTunesPipe_Reading = FALSE;
	BOOL dllPipe_Reading = FALSE;
	BYTE iTunePipe_Buffer[4 * 1024];
	BYTE dllPipe_Buffer[4 * 1024];
	DWORD iTunePipe_Buffer_len = 0;
	DWORD dllPipe_Buffer_len = 0;
	DWORD err = NO_ERROR;
	logIt("start proxy: ++");
	while (!done)
	{
		if (WaitForSingleObject(hThread, 25) == WAIT_OBJECT_0)
		{
			logIt("Apple dll thread terminated.");
			done = TRUE;
		}
		if (iTunesPipe_Reading)
		{
			// readfile has been called, check GetOverlappedResult
			if (WaitForSingleObject(oiTunesPipe.hEvent, 0) == WAIT_OBJECT_0)
			{
				iTunesPipe_Reading = FALSE;
				ResetEvent(oiTunesPipe.hEvent);
				if (GetOverlappedResult(iTunePipe, &oiTunesPipe, &iTunePipe_Buffer_len, TRUE))
				{
					logIO(String::Format("read from iTunes Pips({0})", iTunePipe_Buffer_len), gcnew String((char*)iTunePipe_Buffer, 0, iTunePipe_Buffer_len));
					write_pipe(dllPipe, iTunePipe_Buffer, iTunePipe_Buffer_len);
				}
				else
				{
					//error.
				}
			}
			else
			{
				// still waiting.
			}
		}
		else
		{
			iTunePipe_Buffer_len = 0;
			if (ReadFile(iTunePipe, iTunePipe_Buffer, sizeof(iTunePipe_Buffer), &iTunePipe_Buffer_len, &oiTunesPipe))
			{
				logIO(String::Format("READ from iTunes Pips({0})", iTunePipe_Buffer_len), gcnew String((char*)iTunePipe_Buffer, 0, iTunePipe_Buffer_len));
				// read done
				write_pipe(dllPipe, iTunePipe_Buffer, iTunePipe_Buffer_len);
			}
			else
			{
				err = GetLastError();
				if (err == ERROR_IO_PENDING)
				{
					iTunesPipe_Reading = TRUE;
				}
			}
		}
		if (dllPipe_Reading)
		{
			// readfile has been called, check GetOverlappedResult
			if (WaitForSingleObject(oDllPipe.hEvent, 0) == WAIT_OBJECT_0)
			{
				dllPipe_Reading = FALSE;
				ResetEvent(oDllPipe.hEvent);
				if (GetOverlappedResult(dllPipe, &oDllPipe, &dllPipe_Buffer_len, TRUE))
				{
					logIO(String::Format("read from restore Pips({0})", dllPipe_Buffer_len), gcnew String((char*)dllPipe_Buffer, 0, dllPipe_Buffer_len));
					write_pipe(iTunePipe, dllPipe_Buffer, dllPipe_Buffer_len);
				}
				else
				{
					//error.
				}
			}
			else
			{
				// still waiting.
			}
		}
		else
		{
			dllPipe_Buffer_len = 0;
			if (ReadFile(dllPipe, dllPipe_Buffer, sizeof(dllPipe_Buffer), &dllPipe_Buffer_len, &oDllPipe))
			{
				logIO(String::Format("READ from restore Pips({0})", dllPipe_Buffer_len), gcnew String((char*)dllPipe_Buffer, 0, dllPipe_Buffer_len));
				// read done
				write_pipe(iTunePipe, dllPipe_Buffer, dllPipe_Buffer_len);
			}
			else
			{
				err = GetLastError();
				if (err == ERROR_IO_PENDING)
				{
					dllPipe_Reading = TRUE;
				}
			}
		}
	}
	GetExitCodeThread(hThread, &err);
	logIt(String::Format("start proxy: -- ret={0}",err));
	return err;
}
int start(String^ iTunesPipeName)
{
	int ret = ERROR_INVALID_PARAMETER;
	String^ myServerPipeName = String::Format("{0}-1", iTunesPipeName);
	char buf[10 * 1024];
	ZeroMemory(buf, sizeof(buf));
	StringToPointerA(myServerPipeName, buf, MAX_PATH);
	logIt(String::Format(L"CreateNamedPipe: {0}", myServerPipeName));
	HANDLE hMyPipe = CreateNamedPipeA(buf, PIPE_ACCESS_DUPLEX | FILE_FLAG_OVERLAPPED, PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE, 1, 0x400, 0x400, 0, NULL);
	if (hMyPipe != INVALID_HANDLE_VALUE)
	{
		logIt(gcnew String(L"create server pipe succese."));
		HANDLE hThread = waitForDllConnect(hMyPipe, buf);
		if (hThread != NULL)
		{
			// connect to apple server
			logIt(gcnew String(L"connect to iTunes pipe server."));
			ZeroMemory(buf, sizeof(buf));
			StringToPointerA(iTunesPipeName, buf, MAX_PATH);
			HANDLE hMyClient = connectToiTunesPipe(buf);
			ret = startProxy(hMyClient, hMyPipe, hThread);
		}
		else
		{
			logIt(L"fail to start the Apple dll.");
		}
		CloseHandle(hMyPipe);
	}
	else
	{
		ret = GetLastError();
		logIt(String::Format("start: create server pipe error={0} ", ret));
	}
		
	return ret;
}