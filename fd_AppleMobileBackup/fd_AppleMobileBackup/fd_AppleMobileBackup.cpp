#include "stdafx.h"
#include "AppleMobileBackup.h"

#pragma comment(lib, "Advapi32.lib")

using namespace System;

char** prepareArgv(array<System::String ^> ^args)
{
	logIt("prepareArgv: ++");
	char** ret =
		(args == nullptr) ? new char*[1] : new char*[args->Length + 1];
	int idx = 0;
	char* p = new char[MAX_PATH];
	ret[idx++] = p;
	GetModuleFileNameA(NULL, p, MAX_PATH);
	for each (System::String^ s in args)
	{
		logIt(s);
		char* p = new char[s->Length + 16];
		ret[idx++] = StringToPointerA(s, p, s->Length + 8);
	}
	logIt("prepareArgv: --");
	return ret;
}

char * get_apple_support_path(char* module)
{
	char* ret = NULL;
	if (module != NULL)
	{
		HKEY hKey;
		char buf[1024];
		sprintf_s(buf, "SOFTWARE\\Apple Inc.\\%s", module);
		if (RegOpenKeyExA(HKEY_LOCAL_MACHINE, buf, 0, KEY_READ, &hKey) == ERROR_SUCCESS)
		{
			DWORD size = MAX_PATH;
			ZeroMemory(buf, sizeof(buf));
			if (RegQueryValueExA(hKey, "InstallDir", NULL, NULL, (LPBYTE)buf, &size) == ERROR_SUCCESS)
			{
				ret = _strdup(buf);
			}
			RegCloseKey(hKey);
		}
	}
	return ret;
}

int callLibrary(int argc, char** argv)
{
	logIt("callLibrary: ++");
	int ret = 0;
	char* aasPath =  get_apple_support_path("Apple Application Support");
	if (aasPath != NULL)
	{
		SetDllDirectoryA(aasPath);
	}
	//SetDllDirectoryA("C:\\Program Files (x86)\\Common Files\\Apple\\Apple Application Support");
	HMODULE h = LoadLibraryA("AppleMobileBackup_main.dll");
	if (h != NULL)
	{
		FARPROC fp = GetProcAddress(h, "main");
		logIt(String::Format(L"callLibrary: address of main is {0:X}", (DWORD)fp));

		//HMODULE hh = LoadLibrary(_T("DeviceLink.dll"));
		//if (hh!=NULL)
		//{
		//	hook_iTunesMobileDevice_apis(hh);
		//}
		//
		ret = ((int(*)(int, char**))fp)(argc, argv);
	}
	else
	{
		ret = GetLastError();
	}
	logIt(String::Format("callLibrary: -- ret={0}", ret));
	return ret;
}

int callAppleLibrary(array<System::String ^> ^args)
{
	int ret = -1;
	logIt("callAppleLibrary: ++");
	if (args != nullptr && args->Length > 0)
	{
		int argc = args->Length + 1;
		char** argv = prepareArgv(args);
		ret = callLibrary(argc, argv);
	}
	else
		ret = ERROR_INVALID_PARAMETER;
	logIt(String::Format("callAppleLibrary: -- ret={0}", ret));
	return ret;
}