#include "stdafx.h"
#include <CoreFoundation/CFPropertyList.h>

#include "AppleMobileBackup.h"

using namespace System;
using namespace System::Diagnostics;
using namespace System::Runtime::InteropServices;

char* progressStatus = "<?xml version=\"1.0\" encoding=\"UTF - 8\"?><!DOCTYPE plist PUBLIC \" -//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\"><plist version=\"1.0\"><dict><key>AMSBackupPercentageKey</key><integer>%d</integer></dict></plist>";
char* progressEnd = "<?xml version=\"1.0\" encoding=\"UTF - 8\"?><!DOCTYPE plist PUBLIC \" -//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\"><plist version=\"1.0\"><dict/></plist>";

DWORD read_pipe(HANDLE h, LPBYTE data, int len, int* read)
{
	logIt(gcnew String(L"read_pipe ++"));
	DWORD ret = NO_ERROR;
	OVERLAPPED o1;
	ZeroMemory(&o1, sizeof(OVERLAPPED));
	o1.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	DWORD i;
	BOOL b = ReadFile(h, data, len, &i, &o1);
	if (b)
	{
		*read = i;
		// read success
		logIt(gcnew String(L"read_pipe success"));
	}
	else
	{
		ret = GetLastError();
		if (ret == ERROR_IO_PENDING)
		{
			logIt(gcnew String(L"read_pipe ERROR_IO_PENDING"));
			ret = WaitForSingleObject(o1.hEvent, INFINITE);
			if (ret == WAIT_OBJECT_0)
			{
				logIt(gcnew String(L"read_pipe GetOverlappedResult"));
				b = GetOverlappedResult(h, &o1, &i, TRUE);
				if (b)
				{
					logIt(String::Format(L"read_pipe success read={0}", i));
					*read = i;
					ret = NO_ERROR;
				}
				else
				{
					// get result fail
					ret = GetLastError();
					logIt(String::Format(L"read_pipe GetOverlappedResult error={0}", ret));
				}
			}
			else
			{
				// wait fail
			}
		}
		else
		{
			logIt(gcnew String(L"read_pipe failed"));
		}
	}
	CloseHandle(o1.hEvent);
	if (*read > 0)
		logIt(String::Format("read: {0}", gcnew String((char*)data, 0, *read)));
	logIt(String::Format(L"read_pipe -- ret={0}", *read));
	return ret;
}

DWORD write_pipe(HANDLE h, LPBYTE data, int len)
{
	DWORD ret = NO_ERROR;
	OVERLAPPED o1;
	ZeroMemory(&o1, sizeof(OVERLAPPED));
	o1.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	DWORD i;
	logIt(String::Format("write: {0}", gcnew String((char*)data, 0, len)));
	BOOL b = WriteFile(h, data, len, &i, &o1);
	if (!b)
	{
		ret = WaitForSingleObject(o1.hEvent, INFINITE);
		if (ret == WAIT_OBJECT_0)
		{
			b = GetOverlappedResult(h, &o1, &i, TRUE);
			if (b)
			{
				if (i == len)
				{
					// good.
					ret = NO_ERROR;
				}
				else
				{
					// error
				}
			}
			else
			{
				// error
			}
		}
		else
		{
			// wait fail 
		}
	}
	else
	{
		// write done.
	}
	CloseHandle(o1.hEvent);
	logIt(String::Format(L"write_pipe -- ret={0}", i));
	return ret;
}

void RebootDevice()
{
	logIt("RebootDevice ++");
	if (g_args.udid.size() > 0){
		Process^ coreutil = gcnew Process();
		//inialize 
		coreutil->StartInfo->FileName = Environment::ExpandEnvironmentVariables("%APSTHOME%\\phonedll\\PST_APE_UNIVERSAL_USB_FD\\resource\\iDeviceUtilCore.exe");
		logIt("RebootDevice ++ " + coreutil->StartInfo->FileName);
		coreutil->StartInfo->Arguments = String::Format("-r 1 -u {0}", gcnew String(g_args.udid.c_str()));
		logIt("RebootDevice ++ " + coreutil->StartInfo->Arguments);
		coreutil->StartInfo->CreateNoWindow = true;
		coreutil->StartInfo->WindowStyle = ProcessWindowStyle::Hidden;
		coreutil->StartInfo->UseShellExecute = false;
		//start the process
		coreutil->Start();

		//wait 1000 secounds and kill it
		coreutil->WaitForExit(10000);
		if (!coreutil->HasExited)
		{
			coreutil->Kill();
		}
	}

}

int fake_start(String^ iTunesPipeName)
{
	char buf[10 * 1024] = { 0 };
	int ret = ERROR_INVALID_PARAMETER;
	ZeroMemory(buf, sizeof(buf));
	StringToPointerA(iTunesPipeName, buf, MAX_PATH);
	logIt(String::Format(L"connectToiTunesPipe: {0}", iTunesPipeName));
	HANDLE hMyClient = CreateFileA(buf, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, NULL);
	if (hMyClient != INVALID_HANDLE_VALUE)
	{
		int sz = 0;
		DWORD d = PIPE_READMODE_MESSAGE;
		BOOL b = SetNamedPipeHandleState(hMyClient, &d, NULL, NULL);
		ZeroMemory(buf, sizeof(buf));
		sz = 0;
		read_pipe(hMyClient, (LPBYTE)buf, sizeof(int), &sz);
		logIt(String::Format(L"command lens({0})", sz));
		sz = *(int *)buf;
		read_pipe(hMyClient, (LPBYTE)buf, sz, &sz);
		logIt(String::Format(L"command({0}): {1}", sz, gcnew String(buf, 0, sz)));
		String^ sUUID = Environment::GetEnvironmentVariable("udid");
		if (String::IsNullOrEmpty(sUUID))
		{
			CFDataRef buffer_as_data = CFDataCreate(NULL, (UInt8*)buf, sz);
			if (buffer_as_data == NULL)
			{
				logIt("failed to allocate tm_dialog_output_data\n");
			}
			CFStringRef error = NULL;
			CFPropertyListRef plistref = CFPropertyListCreateFromXMLData(CFAllocatorGetDefault(), buffer_as_data, kCFPropertyListImmutable, &error);
			if (CFGetTypeID(plistref) == CFDictionaryGetTypeID())
			{
				if (CFDictionaryContainsKey((CFDictionaryRef)plistref, CFSTR("AMSTargetIdentifierKey")))
				{
					CFStringRef cfuuid = (CFStringRef)CFDictionaryGetValue((CFDictionaryRef)plistref, CFSTR("AMSTargetIdentifierKey"));
					if (cfuuid != NULL)
					{
						char c_value[512] = { 0 };
						CFStringGetCString(cfuuid, c_value, 510, kCFStringEncodingUTF8);
						g_args.udid = c_value;
					}
					else
					{
						logIt("Plist not include AMSTargetIdentifierKey value.\n");
					}

				}
				else{
					logIt("Plist not include AMSTargetIdentifierKey error.\n");
				}
			}
			else{
				logIt("Plist info error.\n");
			}
		}
		else{
			IntPtr p = Marshal::StringToHGlobalAnsi(sUUID);
			char* newCharStr = static_cast<char*>(p.ToPointer());
			g_args.udid = newCharStr;
		}
		

		if (sz > 0) 
		{
			for (int i = 1; i <= 100; i++)
			{
				//Sleep(500);
				ZeroMemory(buf, sizeof(buf));
				sz = sprintf_s(buf, 1024, progressStatus, i);
				logIt(String::Format(L"Write({0}): {1}", sz, gcnew String(buf, 0, sz)));
				write_pipe(hMyClient, (LPBYTE)&sz, 4);
				write_pipe(hMyClient, (LPBYTE)buf, sz);
			}
			// send end
			ZeroMemory(buf, sizeof(buf));
			sz = sprintf_s(buf, 1024, progressEnd);
			logIt(String::Format(L"Write({0}): {1}", sz, gcnew String(buf, 0, sz)));
			write_pipe(hMyClient, (LPBYTE)&sz, 4);
			write_pipe(hMyClient, (LPBYTE)buf, sz);
		}
		CloseHandle(hMyClient);
		ret = NO_ERROR;
		Sleep(1000);
		RebootDevice();
	}
	else
	{
		logIt(String::Format("Fail to connect iTunes pipe: err={0}", GetLastError()));
	}

	return ret;
}

void fake_test()
{
	char buf[10 * 1024];
	int sz = 0;
	int sz1;
	ZeroMemory(buf, sizeof(buf));
	for (int i = 1; i <= 100; i++)
	{
		sz = sprintf_s(buf, 1024, progressStatus, i);
		sz1= sprintf_s(&buf[2048], 32, "%X", sz);
		logIt(String::Format(L"Write({0}): {1}", sz1, gcnew String(&buf[2048], 0, sz1)));
		logIt(String::Format(L"Write({0}): {1}", sz, gcnew String(buf, 0, sz)));
	}
	
}
